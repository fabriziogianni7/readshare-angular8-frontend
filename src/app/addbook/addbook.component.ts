import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Book } from '../app.module';
@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})

export class AddbookComponent implements OnInit {
  public add:boolean = false;
  public buttonName:any = 'Add'; /*related to ngIf*/

  constructor(private apiService: ApiService) { }
  book:Book;
  toggle(){
    this.add =  !this.add;
    // CHANGE THE NAME OF THE BUTTON.
    if(this.add)
      this.buttonName = "Hide";
    else
      this.buttonName = "Add";
  }

  ngOnInit() {
  }
  showMsg: boolean = false;
  addBook(book){
    console.log(book);
    this.apiService.addBook(book).subscribe((response)=>{
      console.log(JSON.stringify(response));
      this.showMsg= true;
    })

  }

}
