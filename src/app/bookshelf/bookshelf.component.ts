import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-bookshelf',
  templateUrl: './bookshelf.component.html',
  styleUrls: ['./bookshelf.component.css']
})
export class BookshelfComponent implements OnInit {
  displayedColumns: string[] = ['title', 'author', 'year', 'genre', 'email' ];
  books = null;
 // dataSource = null;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
		this.apiService.returnAllBooks().subscribe((data: any[]) => {
        console.log(data);
        this.books = new MatTableDataSource(data);
       // this.dataSource =  new MatTableDataSource(this.books);
	  	});
		};

    applyFilter(event: Event) {

        const filterValue = (event.target as HTMLInputElement).value;
        this.books.filter = filterValue;


  }

}
