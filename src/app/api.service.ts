import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Book } from './app.module';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private SERVER_URL  = 'http://localhost:8080';
  private ALLBOOKS = '/returnAllBooks';
  private ADDBOOK = '/saveBook';

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }


  public returnAllBooks() {
    return this.httpClient.get(this.SERVER_URL + this.ALLBOOKS).pipe(catchError(this.handleError));
  }

  public addBook(book) {
    return this.httpClient.post(this.SERVER_URL + this.ADDBOOK,book,{
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(catchError(this.handleError));
  }



}
